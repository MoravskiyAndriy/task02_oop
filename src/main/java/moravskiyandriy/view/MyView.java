package moravskiyandriy.view;

import moravskiyandriy.controller.*;
import moravskiyandriy.model.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "  1 - print All Goods");
        menu.put("2", "  2 - sort by price");
        menu.put("3", "  3 - sort by category");
        menu.put("4", "  4 - get list of goods from category");
        menu.put("5", "  5 - get goods by name");
        menu.put("6", "  6 - get goods cheaper then");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<String, Printable>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    public final void pressButton1() {
        for (Item it : controller.getAllItems()) {
            System.out.println(it);
        }
    }

    private void pressButton2() {
        for (Item it : controller.getSortedItemListByPrices()) {
            System.out.println(it);
        }
    }

    private void pressButton3() {
        for (Item it : controller.getSortedItemListByCategory()) {
            System.out.println(it);
        }
    }

    private void pressButton4() {
        System.out.println("Please input category (Instrument/Plumbing/Material):");
        String category = input.nextLine();
        for (Item it : controller.getItemListByCategory(category)) {
            System.out.println(it);
        }
    }

    private void pressButton5() {
        System.out.println("Please input some name:");
        String name = input.nextLine();
        for (Item it : controller.getItemByName(name)) {
            System.out.println(it);
        }
    }

    private void pressButton6() {
        System.out.println("Please input price:");
        String price = input.nextLine();
        for (Item it : controller.getItemCheaperThen(Double.parseDouble(price))) {
            System.out.println(it);
        }
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public final void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
