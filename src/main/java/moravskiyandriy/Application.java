package moravskiyandriy;

import moravskiyandriy.view.*;

public final class Application {
    private Application() {
    }

    public static void main(final String[] args) {
        new MyView().show();
    }
}
