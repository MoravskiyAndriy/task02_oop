package moravskiyandriy.model;

public class Plumbing extends Item {
    public Plumbing(final String name,
                    final double price, final String description) {
        super(name, price, description);
        this.category = Category.PLUMBING;
    }
}
