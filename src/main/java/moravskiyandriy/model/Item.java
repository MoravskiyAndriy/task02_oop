package moravskiyandriy.model;

public abstract class Item {
    private String name;
    private double price;
    private String description;
    protected Category category;

    public Item(final String name, final double price, final String description) {
        this.setName(name);
        this.setDescription(description);
        this.setPrice(price);
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final String getDescription() {
        return description;
    }

    public final void setDescription(final String description) {
        this.description = description;
    }

    public final double getPrice() {
        return price;
    }

    public final void setPrice(final double price) {
        this.price = price;
    }

    public final Category getCategory() {
        return category;
    }

    public final String toString() {
        return getName() + "(" + getCategory() + ")\n" + "price: " + getPrice() + "\n" + getDescription() + "\n";
    }

}
