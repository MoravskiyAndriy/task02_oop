package moravskiyandriy.model;

import java.util.List;

public class BusinessLogic implements Model {
    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    public final List<Item> getItemListByCategory(final String category) {
        if (category.toUpperCase().equals("INSTRUMENT")
                || category.toUpperCase().equals("MATERIAL")
                || category.toUpperCase().equals("PLUMBING")) {
            return domain.getItemListByCategory(category);
        }
        return domain.getItemList();
    }

    public final List<Item> getSortedItemListByPrices() {
        domain.sortItemListByPrice();
        return domain.getItemList();
    }

    public final List<Item> getSortedItemListByCategory() {
        domain.sortItemListByCategory();
        return domain.getItemList();
    }

    public final List<Item> getItemCheaperThen(final double price) {
        domain.getItemCheaperThen(price);
        return domain.getItemList();
    }

    public final List<Item> getItemByName(final String name) {
        domain.getItemByName(name);
        return domain.getItemList();
    }

    public final List<Item> getAllItems() {
        domain.getAllItems();
        return domain.getItemList();
    }

}

