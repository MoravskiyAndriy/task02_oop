package moravskiyandriy.model;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Domain {

    private List<Item> itemList;

    public Domain() {
        generateItemList();
    }

    private void generateItemList() {
        itemList = new LinkedList<Item>();

        itemList.add(new Material("paint EcoCristal EcoCristal IР-231",
                75.98,
                "acrylic paint, water emulsion, for walls, for ceiling, colour: white, 1 l."));
        itemList.add(new Plumbing("wash basin Q-TAP Kalao WHI 4115/F008",
                2513,
                "Round ceramic white wash basin."));
        itemList.add(new Plumbing("wash basin GROHE Euro Ceramic",
                2848,
                "hanging porcelain semicircular wash basin."));
        itemList.add(new Instrument("Impact drill Total TG111136",
                1595,
                "power: 1010 W, maximum rotation speed: 2800 rpm, drill diameter: 1.5-13 mm.",
                1010));
        itemList.add(new Instrument("Impact drill Dnipro-M HD-120",
                972,
                "power: 1200 W, maximum rotation speed: 2600 rpm, drill diameter: 1.5-25 mm.",
                1200));
        itemList.add(new Plumbing("wash basin Q-TAP Amadina WHI 3049D/F008",
                2885,
                "semi pedestal ceramic wash basin with standart design."));
        itemList.add(new Plumbing("wash basin CERASTYLE ONE 46x46 см Квадратна (76000)",
                1978,
                "Square veramic white wash basin."));
        itemList.add(new Instrument("Angle grinder Makita GA5030",
                1427,
                "power: 720 W, maximum rotation speed: 11000 rpm, grinding disk diameter: 125 mm.",
                720));
        itemList.add(new Instrument("handsaw Stanley FatMax Jet-Cut",
                992,
                "length of the saw blade: 500 mm, number of teeth per inch: 7.",
                0));
        itemList.add(new Material("paint Sniezka Ultra Biel",
                170.28,
                "acrylic paint, water emulsion, for walls, for ceiling, colour: white, 5 l (7 kg)."));
        itemList.add(new Plumbing("wash basin FANCY MARBLE Lily 600 6307101 ",
                2699,
                "artificial marble white square wash basin."));
        itemList.add(new Material("paint Siltek INTERIOR PRESTIGE-7",
                128.02,
                "latex, water emulsion, for kitchens and bathrooms, colour: white, 0.9 l."));
        itemList.add(new Material("Particleboard laminated SVIS KRONO 2440х1830х16 mm alder 9310 PR",
                741,
                "grade: Higher (extra), lamination: two sides."));
        itemList.add(new Instrument("Hammer Tolsen",
                187,
                "mass: 1 kg, handle material: fiberglass, head material: steel.",
                0));
        itemList.add(new Material("Particleboard polished Fanplit 2440х1220х2,5 mm",
                99,
                "еype of coating: matt, type: sanded."));
    }

    public final void sortItemListByPrice() {
        itemList.sort(Comparator.comparing(Item::getPrice));
    }

    public final void sortItemListByCategory() {
        itemList.sort(Comparator.comparing(Item::getCategory));
    }

    public final List<Item> getItemList() {
        return itemList;
    }

    public final List<Item> getItemListByCategory(final String category) {
        generateItemList();
        List<Item> itemCategoryList = new LinkedList<Item>();
        for (Item it : itemList) {
            if (it.getCategory().toString().equals(category.toUpperCase())) {
                itemCategoryList.add(it);
            }
        }
        itemList = itemCategoryList;
        return itemList;
    }

    public final List<Item> getItemCheaperThen(final double price) {
        List<Item> cheaperItemList = new LinkedList<Item>();
        for (Item it : itemList) {
            if (it.getPrice() < price) {
                cheaperItemList.add(it);
            }
        }
        itemList = cheaperItemList;
        return itemList;
    }

    public final List<Item> getItemByName(final String name) {
        generateItemList();
        List<Item> itemByNameList = new LinkedList<Item>();
        for (Item it : itemList) {
            if (it.getName().toUpperCase().contains(name.toUpperCase())) {
                itemByNameList.add(it);
            }
        }
        itemList = itemByNameList;
        return itemList;
    }

    public final List<Item> getAllItems() {
        generateItemList();
        return itemList;
    }

}