package moravskiyandriy.model;

public class Material extends Item {
    public Material(final String name,
                    final double price, final String description) {
        super(name, price, description);
        this.category = Category.MATERIAL;
    }
}
