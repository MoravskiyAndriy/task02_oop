package moravskiyandriy.model;

import java.util.List;

public interface Model {
    List<Item> getSortedItemListByPrices();

    List<Item> getSortedItemListByCategory();

    List<Item> getItemListByCategory(String category);

    List<Item> getItemCheaperThen(double price);

    List<Item> getItemByName(String name);

    List<Item> getAllItems();
}
