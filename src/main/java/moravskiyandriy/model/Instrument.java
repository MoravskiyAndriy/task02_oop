package moravskiyandriy.model;

public class Instrument extends Item {
    private int power;

    public final int getPower() {
        return power;
    }

    public final void setPower(final int power) {
        this.power = power;
    }

    public Instrument(final String name, final double price,
                      final String description, final int power) {
        super(name, price, description);
        setPower(power);
        this.category = Category.INSTRUMENT;
    }
}
