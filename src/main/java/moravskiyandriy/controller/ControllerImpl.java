package moravskiyandriy.controller;

import moravskiyandriy.model.*;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    public final List<Item> getItemListByCategory(final String category) {
        return model.getItemListByCategory(category);
    }

    public final List<Item> getSortedItemListByPrices() {
        return model.getSortedItemListByPrices();
    }

    public final List<Item> getSortedItemListByCategory() {
        return model.getSortedItemListByCategory();
    }

    public final List<Item> getItemCheaperThen(final double price) {
        return model.getItemCheaperThen(price);
    }

    public final List<Item> getItemByName(final String name) {
        return model.getItemByName(name);
    }

    public final List<Item> getAllItems() {
        return model.getAllItems();
    }
}