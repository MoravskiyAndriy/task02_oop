package moravskiyandriy.controller;

import moravskiyandriy.model.Item;

import java.util.List;

public interface Controller {
    List<Item> getSortedItemListByPrices();

    List<Item> getSortedItemListByCategory();

    List<Item> getItemListByCategory(String category);

    List<Item> getItemCheaperThen(double price);

    List<Item> getItemByName(String name);

    List<Item> getAllItems();
}
